# Mémo Git

# Me
```
git config --global user.name "sib"
git config --global user.email "sibfactory@protonmail.com"
```

## 1- Base

__Version de Git installée__

```
git --version

```

Configurer ses nom et prénom (ou pseudo) et son email
```
git config --global user.name "mon_pseudo"
git config --global user.email mon_email@email.com
```

Coloration syntaxique
```
git config --global color.diff auto
git config --global color.status auto
git config --global color.branch auto
```

Info sur la config ```git config -l ```

## 2- Commande Terminal

commande ls, cd pour aller au dossier créer
mkdir le_nom_du_dossier (si besoin de créer un dossier)

open . (pour ouvir le dossier)


## 3- Commande Git
__Configurer un dossier comme repository Git__
se mettre dans le dossier et taper
```
git init
```

__Créer un commit__ ```git commit --message "Start Dev"```

__Voir les dernières modifications__
```
git status
git diff (voir ce qui a changé)
ou
git diff --color-words (comparaison des fichiers ligne par ligne)
```

__Ajouter un fichier__
```git add le_nom_du_fichier.php```

__Faire un commit__
```git commit --message "Ma modification"```

__On peut vérifier par un__
```git status```

__Affiche les logs__
```
git log
ou
git log --oneline (version courte des log)
```

__Liste des branches et/ou vérifier la branche dans laquelle vous êtes__
```git branch```

__Créer une nouvelle branche__
```git branch le_nom_de_ma_branche```

__Passer d'une branche à une autre__
```git checkout le_nom_de_ma_branche```

__Voir les diff entre les 2 branches__
```git diff master.. le_nom_de_ma_branche```

__Pour merger le code dans la branche master__
```git merge le_nom_de_ma_branche```

__Pour supp la branche qui ne sert plus__
```git branch -d le_nom_de_ma_branche```

__Ajouter tous les fichiers d'un coup__
```git add . ```

__La liste des commits__
```git commit --all```

__Publier les changements sur GitLab/GitHub__
```git push origin le_nom_de_ma_branche```

__Récupérer les changements effectués (utile quand plusieurs personnes développent en même temps)__
```git pull origin le_nom_de_ma_branche```

##  Nouveau dépôt
Git global setup
```
git config --global user.name "Dave Le Dev"
git config --global user.email "miguel.r@infoconception.fr"
```
A partir d'un repository GitLab
```
Récupérer la dernière version du repository de GitLab
git clone https://gitlab.com/LeDev/mon-projet.git
cd mon-projet
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```
Le cas où le dossier existe déjà sur la machine
```
cd existing_folder
git init
Connecter le repository local à celui de GitLab
git remote add origin https://gitlab.com/LeDev/mon-projet.git
git add .
git commit -m "Initial commit"
git push -u origin master
```
Existing Git repository
```
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/LeDev/mon-projet.git
git push -u origin --all
git push -u origin --tags
```
